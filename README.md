# PRUEBA_ELCOMERCIO
Proyecto de prueba de desarrollador FRONT_END.

## Getting Started
Clonar el proyecto y abrir el archivo index.html en un navegador (Chrome, Firefox, Opera, etc.)


## Built With

* [HTML](https://developer.mozilla.org/es/docs/Web/HTML) - Usado para construir la interfaz.
* [JS](https://www.w3schools.com/js/default.asp) - Usado para la programación del lado del cliente.
* [CSS3](https://developer.mozilla.org/es/docs/Web/CSS/CSS3) - Usado para darle estilos al sitio.
* [AngularJS](https://angularjs.org/) - Usado para mejorar la performance del sitio y manejar eficientemente el DOM.
