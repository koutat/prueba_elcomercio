var sortApp = angular.module('sortApp', []);

sortApp.controller('SortListController', function SortListController($scope) {

    //Definimos variabales globales
    $scope.array = [];
    $scope.orderedList = [];
    $scope.number = null;
    //Funcion para agregar los números enteros al array.  entrada: un o mas números separados por comas
    $scope.addToArray = function (param) {
        if(param){
            var arrayNumber = param.split(",");
            arrayNumber.forEach(number => {
                //Hacemos una busqueda para evitar agregar un número ya en la lista
                if(number){
                    var found = $scope.array.find(function (element) {
                        return element == number;
                    });
                    //Si el número no es encontrado se agrega a la lista
                    if (!found)
                        $scope.array.push(parseInt(number));
                }
            });
        } else {
            alert("Ingrese un número!!");
        }
    };

    //Funcion para llamar al ordenamiento
    $scope.sortArray = function(){
        if($scope.array.length > 0){
            $scope.orderedList = sort($scope.array);
        } else {
            alert("La lista esta vacia!!");
        }
    };

    //Funcion para limpiar el array
    $scope.clearArray = function(sortForm) {
        $scope.array = [];
        $scope.orderedList = [];
        $scope.number = null;
        sortForm.$setPristine();
        sortForm.$setUntouched();
    }   

    //Funcion de ordenamiento usando el algoritmo de mezcla o llamado mergeSort
    var sort = function (array) {
        var len = array.length;
        if (len < 2) {
            return array;
        }
        var pivot = Math.ceil(len / 2);
        //Llamada recursiva del algoritmo de ordenamiento, aqui dividimos la lista a la mitad aprox
        return merge(sort(array.slice(0, pivot)), sort(array.slice(pivot)));
    };

    //Funcion Merge para el ordenamiento
    var merge = function (left, right) {
        var result = [];
        while ((left.length > 0) && (right.length > 0)) {
            if (left[0] > right[0]) {
                result.push(right.shift());
            }
            else {
                result.push(left.shift());
            }
        }
        //retornamos las sublistas concatenadas y ordenadas.
        result = result.concat(right, left);
        return result;
    };
});